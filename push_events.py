'''
Push the events to Redis
'''
import simplejson as json
import redis

redisconn = None

def create_conn():
    '''
    Redis DB connection
    '''
    global redisconn
    try:
        pool = redis.ConnectionPool(host='127.0.0.1', port=6379, db=0)
        redisconn = redis.Redis(connection_pool=pool)
    except redis.AuthenticationError, ex:
        print 'TTT' % ex

def lpush_operation():
    '''
    pushing the data into Redis
    '''
    global redisconn

    events = [{"anonid":"abcd", "pk":"", "sk":"", "cid":100}, {"anonid":"abcd", "pk":"", "sk":"9916", "cid":100}, {"anonid":"bcde", "pk":"", "sk":"7716", "cid":100}, {"anonid":"abcd", "pk":"a@g.com", "sk":"", "cid":100}, {"anonid":"bcde", "pk":"a@g.com", "sk":"", "cid":100}, {"anonid":"efgh", "pk":"", "sk":"8816", "cid":100}, {"anonid":"efgh", "pk":"", "sk":"7716", "cid":100}, ]
    for data in events:
        redisconn.lpush('events', json.dumps(data))

if __name__ == '__main__':
    create_conn()
    lpush_operation()
