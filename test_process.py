import pytest
import process_events
import redis

# pylint: disable=import-error,no-name-in-module
from cassandra.cluster import Cluster
from cassandra.query import tuple_factory, BatchStatement
from cassandra import ConsistencyLevel
from cassandra.auth import PlainTextAuthProvider
# pylint: enable=import-error,no-name-in-module

@pytest.fixture
def get_cassdb_conn():
    '''
    Create Cassandra DB Connection
    '''
    session = None
    try:
        auth_provider = PlainTextAuthProvider(username='cassandra', password='cassandra')
        cluster = Cluster(['127.0.0.1'], auth_provider=auth_provider)
        keyspace = "test"
        session = cluster.connect(keyspace)
        session.row_factory = tuple_factory
        session.consistency_level = ConsistencyLevel.QUORUM
    except BaseException as exc:
        print('The cassandra error: %s', exc)
    return session

@pytest.fixture
def get_redis_conn():
    '''
    Create Redis Connection
    '''
    redis_conn = None
    try:
        pool = redis.ConnectionPool(host='127.0.0.1', port=6379, db=0)
        redis_conn = redis.Redis(connection_pool=pool)
    except BaseException as exc:
        print('The Redis error: %s', exc)
    return redis_conn

def list_cass_data(get_cassdb_conn, cid):
    '''
    List the records for clientid
    '''
    result = None
    cass_sess = get_cassdb_conn
    try:
        ##Need to tune the SELECT query
        sql = "SELECT cid, identifier, type, createdtime, cdp_uid FROM activities WHERE cid = {} ALLOW FILTERING ".format(cid)
        #print('Cassandra SQL: %s', sql)
        result = cass_sess.execute(sql)
    except BaseException as exc:
        print('list records error: %s', exc)

    #for row in result:
    #    print('ROW: %s', row)

    return result


def test_event_data1(get_cassdb_conn, get_redis_conn):
    print('Testing starting.')
    cass_sess = get_cassdb_conn
    redis_conn = get_redis_conn
    cid = 100
    #event = {"anonid":"abcd", "pk":"", "sk":"", "cid":100}
    seq_key = "cdpid_seq"
    events = [
            [{"anonid":"abcd", "pk":"", "sk":"", "cid":100},"abcd",1], 
            [{"anonid":"abcd", "pk":"", "sk":"9916", "cid":100},"9916",1],
            [{"anonid":"bcde", "pk":"", "sk":"7716", "cid":100},"7716",2],
            [{"anonid":"abcd", "pk":"a@g.com", "sk":"", "cid":100},"a@g.com",1],
            [{"anonid":"bcde", "pk":"a@g.com", "sk":"", "cid":100},"a@g.com",1],
            [{"anonid":"efgh", "pk":"", "sk":"8816", "cid":100},"efgh",3],
            [{"anonid":"efgh", "pk":"", "sk":"7716", "cid":100},"efgh",3], ]

    i = 0
    for event in events:
        i += 1
        print("INPUT %s %s, %s, %s,", i, cid, event[0], seq_key)
        process_events.prepare_data(cass_sess, redis_conn, cid, event[0], seq_key)
        rows = list_cass_data(cass_sess, cid)
        res = 0
        for row in rows:
            if row[1] == event[1]:
                print('OUTPUT %s DB Rec:%s', i, row)
                res = row[4]
                break
        ##clientid Matching
        assert res == event[2]

