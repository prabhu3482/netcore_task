"""

This script is for Process records from Redis and Insert into Cassandra

"""
import logging
import time
import copy
import datetime
#from __future__ import print_function
import simplejson as json
import redis

# pylint: disable=import-error,no-name-in-module
from cassandra.cluster import Cluster
from cassandra.query import tuple_factory, BatchStatement
from cassandra import ConsistencyLevel
from cassandra.auth import PlainTextAuthProvider
# pylint: enable=import-error,no-name-in-module

def get_cassdb_conn():
    '''
    Create Cassandra DB Connection
    '''
    session = None
    try:
        auth_provider = PlainTextAuthProvider(username='cassandra', password='cassandra')
        cluster = Cluster(['127.0.0.1'], auth_provider=auth_provider)
        keyspace = "test"
        session = cluster.connect(keyspace)
    except BaseException as exc:
        logging.error('The cassandra error: %s', exc)

    return session

def get_redis_conn():
    '''
    Create Redis Connection
    '''
    redis_conn = None
    try:
        pool = redis.ConnectionPool(host='127.0.0.1', port=6379, db=0)
        redis_conn = redis.Redis(connection_pool=pool)
    except BaseException as exc:
        logging.error('The Redis error: %s', exc)

    return redis_conn

def fetch_events(redis_conn, queue, cnt):
    '''
    Fetch the events from Redis
    '''
    result = []
    try:
        pipeline = redis_conn.pipeline()
        while cnt > 0:
            pipeline.rpop(queue)
            cnt -= 1
        items = pipeline.execute()
        if items:
            for item in items:
                if item:
                    data = copy.deepcopy(json.loads(item))
                    result.append(data)

    except BaseException as exc:
        logging.error('Redis Pipeline error: %s', exc)

    return result

def get_new_cdpid(redis_conn, key):
    '''
    Get Unique Sequencs ID from Redis
    '''
    uniq_id = None
    try:
        uniq_id = redis_conn.incr(key)
    except BaseException as exc:
        logging.error('Redis Sequence error: %s', exc)

    return uniq_id

def get_cdpids_cass_db(cass_sess, cid, input_data):
    '''
    Select cdpid from Table
    '''
    cdpids = {}
    try:
        ##Need to tune the SELECT query
        for k in input_data.keys():
            sql = "SELECT cdp_uid, type FROM activities WHERE cid = {} AND identifier = '{}' ALLOW FILTERING ".format(cid, input_data[k])
            logging.info('Cassandra SQL: %s', sql)
            rows = cass_sess.execute(sql)
            for row in rows:
                cdpids[k] = row[0]
    except BaseException as exc:
        logging.error('Cassandra get cdpids error: %s', exc)

    return cdpids

def duplicate_cdpids_check(cass_sess, cid, identifier, cdpid):
    '''
    Check th duplicate
    '''
    val = None
    try:
        ##Need to tune the SELECT query
        sql = "SELECT cdp_uid  FROM activities WHERE cid = {} AND identifier = '{}' AND cdp_uid = {} ALLOW FILTERING ".format(cid, identifier, cdpid)
        logging.info('Cassandra SQL: %s', sql)
        rows = cass_sess.execute(sql)
        if rows:
            val = rows[0][0]
    except BaseException as exc:
        logging.error('Cassandra duplicate cdpids error: %s', exc)
    logging.info('dup rec val: %s', val)
    return val

def list_cass_data(cass_sess, cid):
    '''
    List the records for clientid
    '''
    result = None
    try:
        ##Need to tune the SELECT query
        sql = "SELECT * FROM activities WHERE cid = {} ALLOW FILTERING ".format(cid)
        logging.info('Cassandra SQL: %s', sql)
        result = cass_sess.execute(sql)
    except BaseException as exc:
        logging.error('list records error: %s', exc)

    for row in result:
        logging.info('ROW: %s', row)

    return result


def insert_cass_db(cass_sess, input_data):
    '''
    Insert into Cassandra
    '''
    sql = cass_sess.prepare('INSERT INTO activities (cid, identifier, type, createdtime, cdp_uid) VALUES (?, ?, ?, ?, ?)')
    batch = BatchStatement(consistency_level=ConsistencyLevel.QUORUM)
    try:
        for rec in input_data:
            logging.info('Cass Rec: %s', rec)
            batch.add(sql, rec)
            logging.info('Data Inserted into the table')

        cass_sess.execute(batch)
    except BaseException as exc:
        logging.error('Cassandra insert error: %s', exc)

def prepare_data(cass_sess, redis_conn, cid, row, seq_key):
    '''
    Preparing Cassandra insert data for new identifiers
    '''
    identifiers = {"anonid":"an", "pk":"em", "sk":"mo"}
    try:
        ##Preparing inputdata for fetch the cdpids from cassandra
        cass_input_data = {}
        cass_insert_data = []
        for k in identifiers:
            val = row.get(k, None)
            if val is not None and val != "":
                cass_input_data[k] = val
                ## Preparing Cassandra Batch semi input data
                rec = [cid, val, identifiers[k], datetime.datetime.now()]
                cass_insert_data.append(rec)
        ## Fetching existed cdpids
        if cass_input_data:
            cdpids = get_cdpids_cass_db(cass_sess, cid, cass_input_data)
            logging.info('Cassandra cdpids: %s', cdpids)
            if not cdpids: ## If not fond from Cassandra, generate from Redis
                new_id = get_new_cdpid(redis_conn, seq_key)
                for rec in cass_insert_data:
                    rec.append(new_id)
            else:
                cass_insert_data = []
                ##finding cdpid, order by anonid, pk and sk
                logging.info('Finding cdpid....')
                cdpid = cdpids.get("anonid", None)

                if cdpid is None:
                    cdpid = cdpids.get("pk", None)

                if cdpid is None:
                    cdpid = cdpids.get("sk", None)
                logging.info('Found cdpid :%s', cdpid)

                cdp_list = []
                for k in identifiers:
                    val = cdpids.get(k, None)
                    if val is None: ## if we found any new identifier in the source event, adding into cassanrda with anonid's cdpid
                        orig_val = row.get(k, None)
                        if orig_val != None and orig_val != "":
                            rec = [cid, orig_val, identifiers[k], datetime.datetime.now(), cdpid]
                            cass_insert_data.append(rec)
                    else:
                        if val != None and val != "":
                            cdp_list.append(val)

                if len(cdp_list) > 1:
                    ##Preparing the data for cdpid combinations, left to right, right to left
                    for idx, val in enumerate(cdp_list):
                        if idx+1 < len(cdp_list):
                            if val != cdp_list[idx+1] and duplicate_cdpids_check(cass_sess, cid, val, cdp_list[idx+1]) is None:
                                rec = [cid, str(val), 'id', datetime.datetime.now(), cdp_list[idx+1]]
                                rec1 = [cid, str(cdp_list[idx+1]), 'id', datetime.datetime.now(), val]
                                cass_insert_data.append(rec)
                                cass_insert_data.append(rec1)

        ## Inserting Batch into Cassandra
        if cass_insert_data:
            insert_cass_db(cass_sess, cass_insert_data)


    except BaseException as exc:
        logging.error('Prepare data error: %s', exc)

def process_events(redis_conn, cass_sess):
    '''
    Process Redis events
    '''
    try:
        ##Initialized Qnames and counter for deque
        queue, cnt, seq_key = "events", 10, "cdpid_seq"

        logging.info('Dequeuing events from Redis queue:%s cnt:%s', queue, cnt)
        ##Fetching the events from Redis
        result = fetch_events(redis_conn, queue, cnt)
        for row in result:
            logging.info('ROW: %s', row)
            cid = row.get("cid", None)
            if cid is None:
                logging.info('Skipping the event cid not found in the event:%s', row)
                continue

            prepare_data(cass_sess, redis_conn, cid, row, seq_key)

    except BaseException as exc:
        logging.error('Process events error: %s', exc)

def main():
    '''
    Run Main Method
    '''
    cass_sess = get_cassdb_conn()
    if cass_sess is None:
        logging.error('Cassandra Connection failed')
        return
    cass_sess.row_factory = tuple_factory
    cass_sess.consistency_level = ConsistencyLevel.QUORUM

    redis_conn = get_redis_conn()
    if redis_conn is None:
        logging.error('Redis Connection failed')
        return
    logging.info('Process Started')
    while 1:
        process_events(redis_conn, cass_sess)
        time.sleep(1)

    return

if __name__ == '__main__':
    logging.basicConfig(filename='myapp.log', level=logging.INFO)
    logging.info('App Started!!!')
    main()
    logging.info('App Exited')
